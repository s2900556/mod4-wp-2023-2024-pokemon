package nl.utwente.mod4.pokemon.model;

import java.time.Instant;

public class NamedEntity {

    public String id;
    public String name;
    public String created;
    public String lastUpDate;

    public NamedEntity(){
        id = null;
        name = null;
        created = null;
        lastUpDate = null;
    }

    public boolean isValid() {
        return id != null && !id.isEmpty();
    }

}
